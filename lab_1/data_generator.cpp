#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#define SPLITTER ';'
#define COMPANY_NAME_MAXLEN 15
#define MODEL_MAXLEN 15
#define NAME_MAXLEN 20
#define CITY_MAXLEN 15

//recommended to compile via qmake as g++ handles both endl and \n incorrectly so data can not be loaded to database

char abc[] = "ABCDEFGHIJKLMNOPQRSTYVWXYZ";

std::string random_string(unsigned size)
{
    std::string name;
    for (unsigned i = 0; i < size; ++i)
        name.push_back(abc[rand() % 26]);
    return name;
}

std::string random_country()
{
    std::string countries[5] = { "Russia", "USA", "UK", "China", "South Korea" };
    return countries[rand() % 5];
}


void generate_manufacturers()
{
    std::ofstream fout("manufacturers.dat");
    for (unsigned id = 0; id < 1000; ++id)
        fout << id << SPLITTER << random_string(COMPANY_NAME_MAXLEN) << SPLITTER << random_country() << SPLITTER << rand() % 5000 << std::endl;
}


void generate_phones()
{
    std::ofstream fout("phones.dat");
    for (unsigned id = 0; id < 1000; ++id)
        fout << id << SPLITTER << random_string(MODEL_MAXLEN) << SPLITTER << rand() % 129 << SPLITTER << rand() % 10 << std::endl;
}


void generate_buyers()
{
    std::ofstream fout("buyers.dat");
    const unsigned halfname_length = NAME_MAXLEN / 2;
    for (unsigned id = 0; id < 1000; ++id)
        fout << id << SPLITTER << random_string(halfname_length - 1) + ' ' + random_string(halfname_length) << SPLITTER << random_country() << SPLITTER << random_string(CITY_MAXLEN) << std::endl;
}


void generate_relations()
{
    std::ofstream fout("relations.dat");
    for (unsigned i = 0; i < 1000; ++i)
        fout << rand() % 1000 << SPLITTER << i << SPLITTER << rand() % 1000 << std::endl;
}


int main()
{
    srand(5);
    int choice;
    void (*generators[])() = { generate_manufacturers, generate_phones, generate_buyers, generate_relations };
    std::cout << "0 - generate all\n";
    std::cout << "1 - generate manufacturers\n";
    std::cout << "2 - generate phones\n";
    std::cout << "3 - generate buyers\n";
    std::cout << "4 - generate relations\n";
    std::cin >> choice;
    if (choice)
    {
        --choice;
        generators[choice]();
    }
    else
        for (int i = 0; i < 4; ++i)
            generators[i]();
}
