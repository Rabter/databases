﻿create database smartphoneMarket;
go

use smartphoneMarket;
go

--create schema MBP;
--go

create table Manufacturers(
	id int NOT NULL primary key,
	name char(15),
	base_country varchar(20),
	capitalization int
	)
go

create table Phones(
	id int NOT NULL primary key,
	model varchar(15),
	memoryGB smallint,
	user_rate tinyint
	)
go

create table Buyers(
	id int NOT NULL primary key,
	name char(20),
	country varchar(20),
	city varchar(15)
	)
go

create table linker(
	Mid int,
	Pid int,
	Bid int,
	foreign key (Mid) references Manufacturers(id),
	foreign key (Pid) references Phones(id),
	foreign key (Bid) references Buyers(id)
	)
go

bulk insert Manufacturers from 'D:\\University\Databases\lab_1\\manufacturers.dat'
with (fieldterminator = ';' );
go

bulk insert Phones from 'D:\\University\Databases\lab_1\\phones.dat'
with (fieldterminator = ';' );
go

bulk insert Buyers from 'D:\\University\Databases\lab_1\\buyers.dat'
with (fieldterminator = ';' );
go

bulk insert linker from 'D:\\University\Databases\lab_1\\relations.dat'
with (fieldterminator = ';' );
go

select * from Manufacturers;
go

select * from Phones;
go

select * from Buyers;
go

select * from linker;
go

truncate table Manufacturers;
go

truncate table Phones;
go

truncate table Buyers;
go

truncate table linker;
go

--For db import
SELECT 'sqlserver' dbms,t.TABLE_CATALOG,t.TABLE_SCHEMA,t.TABLE_NAME,c.COLUMN_NAME,c.ORDINAL_POSITION,c.DATA_TYPE,c.CHARACTER_MAXIMUM_LENGTH,n.CONSTRAINT_TYPE,k2.TABLE_SCHEMA,k2.TABLE_NAME,k2.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLES t LEFT JOIN INFORMATION_SCHEMA.COLUMNS c ON t.TABLE_CATALOG=c.TABLE_CATALOG AND t.TABLE_SCHEMA=c.TABLE_SCHEMA AND t.TABLE_NAME=c.TABLE_NAME LEFT JOIN(INFORMATION_SCHEMA.KEY_COLUMN_USAGE k JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS n ON k.CONSTRAINT_CATALOG=n.CONSTRAINT_CATALOG AND k.CONSTRAINT_SCHEMA=n.CONSTRAINT_SCHEMA AND k.CONSTRAINT_NAME=n.CONSTRAINT_NAME LEFT JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS r ON k.CONSTRAINT_CATALOG=r.CONSTRAINT_CATALOG AND k.CONSTRAINT_SCHEMA=r.CONSTRAINT_SCHEMA AND k.CONSTRAINT_NAME=r.CONSTRAINT_NAME)ON c.TABLE_CATALOG=k.TABLE_CATALOG AND c.TABLE_SCHEMA=k.TABLE_SCHEMA AND c.TABLE_NAME=k.TABLE_NAME AND c.COLUMN_NAME=k.COLUMN_NAME LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE k2 ON k.ORDINAL_POSITION=k2.ORDINAL_POSITION AND r.UNIQUE_CONSTRAINT_CATALOG=k2.CONSTRAINT_CATALOG AND r.UNIQUE_CONSTRAINT_SCHEMA=k2.CONSTRAINT_SCHEMA AND r.UNIQUE_CONSTRAINT_NAME=k2.CONSTRAINT_NAME WHERE t.TABLE_TYPE='BASE TABLE';
go