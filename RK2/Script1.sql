﻿create database RK2;
go

-- Задание 1

create table Cars(
	id int not null primary key,
	brand varchar(20),
	model varchar(20),
	manufacture_date date,
	registration_date date
	)

create table Drivers(
	id int not null primary key,
	license bigint,
	phone char(12),
	name varchar(40),
	car int
	foreign key (car) references Cars(id) -- По схеме в блоке водителей у 1 водителя 1 id автомобиля (и.соответственно, у 1автомобиля может быть несколько водителей)
	                                      --, но по стрелкам схемы у 1 водителя несколько автомобилей. Сделал, ориентируясь на блок.
	)
	
create table Penalties (
	id int not null primary key,
	type varchar(20),
	value int,
	warning varchar(40)
	)
go

create table linker(
	did int,
	pid int,
	foreign key (did) references Drivers(id),
	foreign key (pid) references Penalties(id)
	)
go

insert into Cars values(1, 'Audi', 'Q5', '2019-08-04', '2019-09-30');
insert into Cars values(2, 'BMW', 'X7', '2018-04-20', '2018-06-02');
insert into Cars values(3, 'Folkswagen', 'Tiguan', '2013-05-04', '2015-09-30');
insert into Cars values(4, 'Mercedes', 'CLS', '2019-01-01', '2019-01-02');
insert into Cars values(5, 'Chevrolet', 'Camaro', '2016-05-02', '2016-08-28');
insert into Cars values(6, 'ЗИЛ', '130', '2000-03-07', '2000-03-08');
insert into Cars values(7, 'Nissan', 'Quashkai', '2006-07-09', '2006-07-19');
insert into Cars values(8, 'Jeep', 'Rubicon', '1995-01-01', '1995-02-14');
insert into Cars values(9, 'УАЗ', 'Патриот', '2015-07-21', '2015-07-30');
insert into Cars values(10, 'Opel', 'Zafira', '2017-05-08', '2017-06-09');

insert into Drivers values(1, 598765, '+79055566678', 'Petrov Petr Petrovich', 9);
insert into Drivers values(2, 598766, '+79057654321', 'Petrov Petr Ivanovich', 8);
insert into Drivers values(3, 598767, '+79055566679', 'Petrov Ivan Petrovich', 10);
insert into Drivers values(4, 598768, '+79055566670', 'Petrov Ivan Ivanovich', 7);
insert into Drivers values(5, 598769, '+79051234567', 'Ivanov Petr Petrovich', 1);
insert into Drivers values(6, 598760, '+79055566671', 'Ivanov Petr Ivanovich', 2);
insert into Drivers values(7, 598734, '+79055566625', 'Ivanov Ivan Petrovich', 5);
insert into Drivers values(8, 598733, '+79056666666', 'Ivanov Ivan Ivanovich', 4);
insert into Drivers values(9, 598732, '+79055050105', 'Unexpected Ivan Sidorovich',3);
insert into Drivers values(10, 123456, '+79050000001', 'Petrov Sidr Stepanovich', 6);

-- Простите, я не представляю,что можно писать в предупреждении к уже выписанному штрафу
insert into Penalties values(1, 'Превышение', 800, 'Вмажешься');
insert into Penalties values(2, 'Превышение', 2000, 'Сядешь');
insert into Penalties values(3, 'Неправильная парковка', 200, 'Больше не надо');
insert into Penalties values(4, 'Проезд на красный', 1000, 'В тебя вмажутся');
insert into Penalties values(5, 'Превышение', 1000, 'Все равно опоздаешь');
insert into Penalties values(6, 'Превышение', 1500, 'Не спи, замерзнешь');
insert into Penalties values(7, 'Превышение', 1234, 'Дома ждут');
insert into Penalties values(8, 'Неправильная парковка', 500, 'Без колес останешься');
insert into Penalties values(9, 'Проезд на красный', 2000, 'Опасно');
insert into Penalties values(10, 'Проезд на красный', 3000, 'Вас уже трое');
go

insert into linker values(1,1);
insert into linker values(2,2);
insert into linker values(3,3);
insert into linker values(4,4);
insert into linker values(5,5);
insert into linker values(6,6);
insert into linker values(7,7);
insert into linker values(8,8);
insert into linker values(9,9);
insert into linker values(10,10);

select * from Drivers
go
select * from Penalties
go
select * from Cars
go
select * from linker
go


-- Задание 2
-- Select с предикатом сравнения. Выбрать все штрафы, значения котоых более 500 рублей
select * from Penalties where value > 500;
go

-- Select с оконной функцией. Вывести максимальное и минимальное значения штрафыов
select max(value) max, min(value) min from Penalties;
go

-- Select с подзапросом. Водитель  максимальным штрафом 
select d.id, d.license, d.name, d.phone, d.car from Drivers d join linker l on d.id = l.did join Penalties p on l.pid = p.id
where p.value = (select max(value) from Penalties);
go


-- Задание 3
create procedure drop_triggers
@count int output
as
begin
	select @count = count(*) 
	from sys.triggers t 
	where t.[type] = 'TR';

	declare @sql nvarchar(max) = ''

	select @sql = @sql + 'DROP TRIGGER ' + t.name + '; ' 
	from sys.triggers t 
	where t.[type] = 'TR'

end
go

create trigger notifier on Drivers
after insert, delete, update
as print 'Phones database has been modified';
go

declare @count int = 0;
exec drop_triggers @count output;

select @count;
go