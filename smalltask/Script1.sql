﻿create database smalltask;
go

use smalltask;
go


create table letters(
    id int ,
    letter char(1)
);
go

insert into letters(id, letter) values(1, 'B');
insert into letters(id, letter) values(2, 'C');
insert into letters(id, letter) values(3, 'D');
insert into letters(id, letter) values(4, 'E');
insert into letters(id, letter) values(5, 'F');
insert into letters(id, letter) values(6, 'G');
insert into letters(id, letter) values(7, 'H');
insert into letters(id, letter) values(8, 'I');
insert into letters(id, letter) values(9, 'J');
go


insert into letters(id, letter) values(0, 'A');
go

insert into letters(id, letter) values(NULL, 'A');
go

insert into letters(id, letter) values(-1, 'J');
go

select * from letters;
go

select
case when positives + negatives = total then
((case when negatives % 2 = 0 then 1 else -1 end) * exp(sum(log(abs(id)))))
else 0 end as mul
from letters,
(
select
sum(case when id < 0 then 1 else 0 end) as negatives,
sum(case when id > 0 then 1 else 0 end) as positives,
count(*) as total
from letters
where id is not null
) as npt
where id <> 0
group by positives, negatives, total;
go

--truncate table letters;
--go

--drop table letters;
--go
