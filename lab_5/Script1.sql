﻿use smartphoneMarket;
go

select * from Phones for xml auto;
go
select * from Phones for xml raw;
go
select * from Phones for xml path;
go
select 1 as tag, NULL as parent, id as 'Phones!1!id', model as 'Phones!1!model', memoryGB as 'Phones!1!memoryGB', user_rate as 'Phones!1!user_rate'
from Phones for xml explicit;
go

declare @idoc int, @doc xml;
select @doc = c from openrowset(bulk 'D:\University\Databases\lab_5\phones.xml', single_blob) as temp(c);
exec sp_xml_preparedocument @idoc output, @doc;

select *
from openxml (@idoc, '/Phones/row', 2)
with (id int, model varchar(20), memoryGB int, user_rate tinyint);
exec sp_xml_removedocument @idoc;