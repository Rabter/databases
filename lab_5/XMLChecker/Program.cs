﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.IO;


namespace XMLChecker
{
    class XmlSchemaSetExample
    {
        static int Main()
        {
            Console.Write("Enter xml file path:\n");
            string xmlPath = Console.ReadLine();
            if (!File.Exists(xmlPath))
            {
                Console.Write("File {0} does not exist\n", xmlPath);
                 return 1;
            }
            Console.Write("Enter xsd scheme path:\n");
            string xsdPath = Console.ReadLine();
            if (!File.Exists(xsdPath))
            {
                Console.Write("File {0} does not exist\n", xsdPath);
                return 1;
            }
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(null, xsdPath);
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += new ValidationEventHandler(SettingsValidationEventHandler);

            XmlReader phones = XmlReader.Create(xmlPath, settings);

            while (phones.Read()) { }
            return 0;
        }

        static void SettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
            }
        }
    }
}

