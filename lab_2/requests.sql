﻿use smartphoneMarket;
go

select * from Manufacturers m join Buyers b on m.id = b.id join Phones p on m.id = p.id;
go

select distinct m1.name, m1.capitalization, m2.name, m2.base_country
from Manufacturers m1 left join Manufacturers m2 on m1.base_country = m2.base_country
where m1.id <> m2.id and m1.capitalization > 4900

-- Список производителей с капитализацией от 1000 до 2000 у.е.
select distinct m.id, m.name, m.capitalization
from Manufacturers m
where capitalization between 1000 and 2000;
go

-- Список покупателей, чье имя содержит QW
select distinct b.id, b.name
from Buyers b
where name like '%QW%';
go

-- Список производителей с капитализацией более 4000 из стран, в которых живут покупатели с именем содеожащим QWE
select distinct m.id, m.name, m.base_country, b.name, b.id
from Manufacturers m join linker l on m.id = l.Mid join Buyers b on b.id = l.Bid
where m.base_country in (
	select country
	from Buyers
	where name like '%QWE%'
	) and capitalization > 4000;
go
select id, name, country
from Buyers
where name like '%QWE%'
go
-- Ну или так
select distinct m.id, m.name, m.base_country, b.name, b.id
from Manufacturers m join linker l on m.id = l.Mid join Buyers b on b.id = l.Bid
where m.name in (
	select m.name
	from Manufacturers m join linker l on m.id = l.Mid join Phones p on p.id = l.Pid
	where p.memoryGB = 128
	) and capitalization > 0;
go

-- Список производителей, у которых соввершали покупки жители России
select distinct m1.id, m1.name, m1.base_country
from Manufacturers m1
where exists (
	select b.id
	from Buyers b join linker l on b.id = l.Bid join Manufacturers m2 on m2.id = l.Mid
	where b.country = 'Russia' and m1.id = m2.id
	);
go
select m.id, b. name, b.country
from Manufacturers m join linker l on m.id = l.Mid join Buyers b on b.id = l.Bid
where b.country = 'Russia';

-- Список производителей с капитализацией выше, чем у любого Российского производителя
select m.id, m.name, m.base_country, m.capitalization
from Manufacturers m
where m.capitalization > all (
	select m.capitalization
	from Manufacturers m
	where m.base_country = 'Russia'
	);
go

select avg(country_total) as 'actual', sum(country_total) / count(*) as 'calculated'
from (
	select base_country, sum(capitalization) as country_total
	from Manufacturers
	group by base_country
	) as countries_avg;
go

select m.id, m.name,
(
	select avg(memoryGB)
	from Phones p join linker l on p.id = l.Pid join Manufacturers m1 on m1.id = l.Mid
	where m1.id = m.id
) as average_memory,
(
	select max(memoryGB)
	from Phones p join linker l on p.id = l.Pid join Manufacturers m1 on m1.id = l.Mid
	where m1.id = m.id
) as max_memory
from Manufacturers m;
go

select m.name, m.base_country,
	case (base_country)
		when 'Russia' then 'Europe and Asia'
		when 'UK' then 'Europe'
		when 'USA' then 'Northen America'
		else 'Asia'
	end as 'Area'
from Manufacturers m;
go

select p.model, p.memoryGB,
	case
		when memoryGB < 16 then 'Tiny'
		when memoryGB < 32 then 'Small'
		when memoryGB < 64 then 'Regular'
		when memoryGB < 128 then 'Big'
		else 'Great'
	end as 'Memory appraisal'
from Phones p;
go

drop table #BestRated
go
select p.model, p.user_rate
into #BestRated
from Phones p
where user_rate > (
	select avg(user_rate)
	from Phones
	)
go
select * from #BestRated
go

select id, name, base_country, capitalization, total_sales
from Manufacturers m join
(
	select top 1 m1.id as m1id, count(*) as total_sales
	from Manufacturers m1 join linker l1 on m1.id = l1.Mid
	group by m1.id
	order by total_sales desc
) as best_seller on m.id = m1id;
go

select b.name
from Manufacturers m join linker l on m.id = l.Mid join Buyers b on b.id = l.Bid
where m.id in (
	select m.id
	from Manufacturers m join linker l on m.id = l.Mid join Phones p on p.id = l.Pid
	where p.id in (
		select id
			from Phones
			group by id
			having memoryGB = (
				select max(memoryGB)
				from Phones
				)
		)
);
go

select name, count(m.id) as sales_count
from Manufacturers m join linker l on m.id = l.Mid join Phones p on p.id = l.Pid
group by m.name
go

select name, count(m.id) as sales_count
from Manufacturers m join linker l on m.id = l.Mid join Phones p on p.id = l.Pid
group by m.name
having name like '%QW%'
go

insert Buyers (id, name, country, city) values (1001, 'SIDOROV PETR', 'Russia', 'MOSCOW')
select * from Buyers
where id = 1001;
go

create table TopMemoryPhones(
	Pid int,
	model varchar(15),
	memoryGB smallint,
	user_rate tinyint
);
go

insert TopMemoryPhones (Pid, model, memoryGB, user_rate)
select id, model, memoryGB, user_rate
from Phones p
where p.memoryGB = (
select max(memoryGB)
from Phones
);
select * from TopMemoryPhones;
go

update TopMemoryPhones
set user_rate = 5
where user_rate = 2
select * from TopMemoryPhones;
go

update TopMemoryPhones
set user_rate = (
select avg(user_rate)
from Phones
)
where user_rate = 2
select * from TopMemoryPhones;
go

truncate table TopMemoryPhones;
go
drop table TopMemoryPhones;
go

delete Buyers where id = 1001;
go

select * into tmp from Manufacturers;
go

delete tmp
where base_country in (
select country
from Buyers
where id between 105 and 110
);
go

select * from tmp;
go

drop table tmp;
go

with summerizer(country, count) as
(
	select base_country, count(*)
	from Manufacturers
	group by base_country
)
select max(count) as max_manufacturers_count
from summerizer;
go
-- Созданиетаблицы.
CREATE TABLE dbo.MyEmployees(
EmployeeID smallint NOT NULL,
FirstName nvarchar(30) NOT NULL,
LastName  nvarchar(40) NOT NULL,
Title nvarchar(50) NOT NULL,
DeptID smallint NOT NULL,
ManagerID int NULL,
CONSTRAINT PK_EmployeeID PRIMARY KEY CLUSTERED(EmployeeID ASC)) ;
GO
-- Заполнениетаблицызначениями.
INSERT INTO dbo.MyEmployees VALUES(1, N'Иван', N'Петров', N'Главный исполнительный директор',16,NULL) ;
INSERT INTO dbo.MyEmployees VALUES(2, N'Федор', N'Петров', N'Почти главный исполнительный директор',17,1) ;
INSERT INTO dbo.MyEmployees VALUES(3, N'Иван', N'Сидоров', N'исполнительный директор',18,1) ;
INSERT INTO dbo.MyEmployees VALUES(4, N'Геннадий', N'Иванов', N'младший исполнительный директор',19,2) ;
INSERT INTO dbo.MyEmployees VALUES(5, N'Петр', N'Федоров', N'Прислуга',20,3) ;
GO

-- ОпределениеОТВ
WITH DirectReports (ManagerID, EmployeeID, Title, DeptID,Level) AS (
-- Определениезакрепленногоэлемента
SELECT e.ManagerID, e.EmployeeID, e.Title, e.DeptID, 0 AS Level
FROM dbo.MyEmployees AS e
WHERE ManagerID IS NULL
UNION ALL
-- Определениерекурсивногоэлемента
SELECT e.ManagerID, e.EmployeeID, e.Title, e.DeptID,Level+ 1
FROM dbo.MyEmployees AS e INNER JOIN DirectReports AS d ON e.ManagerID = d.EmployeeID
)
-- Инструкция, использующаяОТВ
SELECT ManagerID, EmployeeID, Title, DeptID,Level
FROM DirectReports ;
go
truncate table MyEmployees;
drop table MyEmployees;
go

with topper (id, model, memoryGB, user_rate) as
(
	select id, model, memoryGB, user_rate
	from Phones
	where user_rate = 9
	union all
	select p.id, p.model, p.memoryGB, p.user_rate
	from Phones p join topper t on p.user_rate = t.user_rate - 1
)
select *  from topper;
go

select m.name,
avg(p.memoryGB) over(partition by m.name)as avg,
min(p.memoryGB) over(partition by m.name)as min,
max(p.memoryGB) over(partition by m.name)as max
from Manufacturers m join linker l on m.id = l.Mid join Phones p on p.id = l.Pid;
go

select id, model, memoryGB, user_rate
from (
select *, row_number() over(partition by model order by (select NULL)) rn
from
(
select *
from Phones
where user_rate > 5
union all
select *
from Phones
where memoryGB < 64
) as subrequest1
) as subrequest2
where rn = 1;
go