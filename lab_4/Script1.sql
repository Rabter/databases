﻿use SmartphoneMarket;
go

exec sp_configure 'clr enabled', 1;  
reconfigure;  
go
alter database SmartphoneMarket set trustworthy on;
go

select * from sys.assemblies
where name='sharp_clr';
go

drop function HighRatedValidMemoryPhonesCount;
drop aggregate geometric_mean;
drop function squared_range;
drop procedure copy_buyers;
drop trigger alter_handler;
drop type dbo.Human;
drop assembly sharp_clr;
go

create assembly sharp_clr from 'D:\University\Databases\lab_4\CLR\bin\Debug\CLR.dll'
with PERMISSION_SET = UNSAFE;
go

-- 1. Scalar. Count phones with user_rate grater than @min_rate and memoryGB as a power of 2 (starting with 16)
create function HighRatedValidMemoryPhonesCount(@min_rate tinyint)
returns int
as
	external name sharp_clr."CLR.ScalarFuncs".HighRatedValidMemoryPhonesCount;
go

declare @minrate int = 5;
select dbo.HighRatedValidMemoryPhonesCount(@minrate) as clr_result;

select * from Phones
where user_rate >= @minrate and memoryGB in (16, 32, 64, 128);
go

-- 2. Aggregate. Returns the geometric meaming of values
create aggregate geometric_mean(@input float)
returns float
external name sharp_clr.[CLR.GeometricMean] -- same as sharp_clr."CLR.GeometricMean"
go

select * from Phones
where id > 0 and id < 5;
select dbo.geometric_mean(id) as clr_result from Phones
where id > 0 and id < 5;
go

-- 3. Table function. Returns a table of all sqared values of the given range
create function dbo.squared_range(@begin int, @end int)
returns table(squared_range int)
as external name sharp_clr.[CLR.TableFuncs].SqrRange;
go

select * from dbo.squared_range(1, 5);
go

-- 4. Stored procedure. Copies all records of buyers from Russia to a given table
create procedure dbo.copy_buyers @table_name nvarchar(20)
as
external name sharp_clr.[CLR.StoredProcedures].CopyBuyers;
go

exec dbo.copy_buyers 'b1';
go

select * from b1;
select * from Buyers b where b.Country = 'Russia';
drop table b1;
go

-- 5. Trigger. Forbids deleting. If the action is insert, creates a copy of a table and inserts data in a copied table
create trigger dbo.alter_handler on Manufacturers
instead of insert, delete
as
external name sharp_clr.[CLR.Triggers].AlterHandler;
go

delete from Manufacturers where id = 999;
select * from Manufacturers where id = 999;
go 

--6. User-defined type. Human(birth date, name, sex)
create type dbo.Human
external name sharp_clr.[CLR.Human];
go

declare @person dbo.Human;
set @person  = cast('01-01-1901;Petrov Daniil Yaroslavovich;Male' as dbo.Human);
select @person.ToString()
select @person.age_at('02-01-1905');
go

