﻿use smartphoneMarket;
go

select * from Manufacturers m join Buyers b on m.id = b.id join Phones p on m.id = p.id;
go

select * from Manufacturers
go
select * from Phones
go
select * from Buyers
go

-- A1 скалярная функция
-- Количество телефонов заданного рейтинга (rate) с количеством памяти не менее заданного (minmem)
create function phone_counter(@rate tinyint, @minmem smallint)
returns int
begin
	declare @cnt int
	select @cnt = count(*)
	from Phones
	where user_rate = @rate and memoryGB >= @minmem;
    return @cnt;
end
go

select dbo.phone_counter(9, 64);
go

select * from Phones
where user_rate = 9 and memoryGB >= 64;
go

-- A2 подставляемая табличная функция
-- Люди, проживающие в заданной стране (country)
create function people_from(@country varchar(20))
returns table
	return (
	    select *
	    from Buyers
		where country = @country
		)
go

select * from dbo.people_from('Russia');
go

select * from Buyers
where country = 'Russia';
go

-- A3 многооператорная табличная функция
-- Производители из country1, затем из country2
create function manufacturers_from(@country1 varchar(20), @country2 varchar(20))
returns @res table (id int, name char(15), country varchar(20))
begin
	insert @res
	select id, name, base_country 
	from Manufacturers
	where base_country  = @country1;
	insert @res
	select id, name, base_country 
	from Manufacturers
	where base_country  = @country2;
	return
end
go

select id, name, base_country from Manufacturers
where base_country = 'Russia'
union select id, name, base_country from Manufacturers
where base_country = 'China';

select * from manufacturers_from('Russia', 'China');
go

-- A4 Рекурсивная функция
-- Функция, вводящая телефоны в порядке убывания рейтинга, начиная с заданного (maxrating)
create function sorted_phones(@maxrating int)
returns @res table (id int, model varchar(15), memoryGB smallint, user_rate tinyint)
begin
	insert @res select *
	from Phones where user_rate = @maxrating
	if @maxrating >= 0
	    insert @res select *
		from sorted_phones(@maxrating - 1);
    return
end
go

select * from sorted_phones(9);
go

-- B1 Хранимая процедура
-- Процедура, устанавливающая user_rate заданного телефона
create procedure set_rate @id int, @new_rate tinyint
as
begin
    update Phones
	set user_rate = @new_rate
	where id = @id;
end
go

select * from Phones where id = 500;
go
set_rate 500, 8;
go
select * from Phones where id = 500;
go

-- B2 Рекурсивная процедура
-- Вставить num записей с заданными атрибутами
create procedure insert_many @num int, @model varchar(15), @memoryGB smallint, @user_rate tinyint
as
begin
    if @num > 0
	begin
        declare @id int
	    select @id = max(id) + 1
	    from Phones;
		insert Phones (id, model, memoryGB, user_rate) values(@id, @model, @memoryGB, @user_rate);
		set @num = @num - 1
		exec insert_many @num, @model, @memoryGB, @user_rate;
    end
end
go

select * from Phones
go
insert_many 4, 'gg', 3, 0;
go
select * from Phones
go
delete from Phones where id >= 1000;
go

-- B3 Процедура с курсором
-- Процедура, выводящая модели всех телефонов
create procedure select_phone_models
as
begin
	declare @msg varchar(20);
    declare cur cursor local
	for select model from Phones
	open cur
	fetch next from cur into @msg;
	print @msg
	while @@fetch_status = 0
	begin
	    fetch next from cur into @msg;
		print @msg
    end
	close cur;
end
go

exec select_phone_models;
go
select model from Phones;
go

-- B4 Процедура доступа к метаданным
-- Получить object_id таблиц Manufacturers, Buyers, Phones и linker
create procedure sys_ids
as
begin
select name, object_id
from sys.objects
where name = 'Manufacturers'
union select name, object_id
from sys.objects
where name = 'Buyers'
union select name, object_id
from sys.objects
where name = 'Phones'
union select name, object_id
from sys.objects
where name = 'linker'
end;
go

exec sys_ids;
go

-- C1 Триггер after
-- Вывести сообщение при изменении таблицы Phones
create trigger notifier on Phones
after insert, delete, update
as print 'Phones database has been modified';
go

insert into Phones (id, model, memoryGB, user_rate) values (1000, 'gg', 64, 9);
update Phones set user_rate = 0 where id = 1000
delete from Phones where id = 1000;
go

-- C2 Триггер instead of
-- Триггер, не дающий добавлять записи в Buyers
create trigger guard on Buyers
instead of insert
as print 'You can not add any records to this table';
go

insert into Buyers (id, name, country, city) values(1000, 'gg', 'wp', 'ez');
select * from Buyers where id = 1000;
go

-- Защита
create table PhonesLog (operation char(10), cnt int);
go
insert into PhonesLog (operation, cnt) values('insert', 0);
insert into PhonesLog (operation, cnt) values('delete', 0);
go

create trigger insertlogger on Phones
after insert
as
update PhonesLog
set cnt = cnt + 1
where operation = 'insert';
go

create trigger deletelogger on Phones
after delete
as
update PhonesLog
set cnt = cnt + 1
where operation = 'delete';
go

select * from PhonesLog;
go

insert_many 4, 'gg', 3, 2;
go

delete Phones where id > 999;
go

drop table PhonesLog;
go